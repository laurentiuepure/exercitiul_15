package ro.orangeprojects;

public class SynchronizationTest {

    public static void main(String[] args) {
        Sender snd = new Sender();
        ThreadedSend t1 = new ThreadedSend(snd, "Hi Terry Williams");
        ThreadedSend t2 = new ThreadedSend(snd, "We hope you enjoy the stay at our hotel!");

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
