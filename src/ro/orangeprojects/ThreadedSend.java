package ro.orangeprojects;

public class ThreadedSend extends Thread {

    private String msg;
    private Thread t;
    Sender sender;

    public ThreadedSend(Sender sender, String msg) {
        this.sender = sender;
        this.msg = msg;
    }


    @Override
    public void run() {

        synchronized (sender) {

            sender.send(this.msg);

        }


    }
}
