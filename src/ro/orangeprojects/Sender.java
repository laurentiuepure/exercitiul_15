package ro.orangeprojects;

public class Sender {

    public void send(String message) {

        System.out.println("Sending " + message);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException x) {
            System.out.println("Interrupted");
        }

        System.out.println(message);
        System.out.println("Sent");

    }
}
